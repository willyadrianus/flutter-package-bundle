import 'dart:typed_data';

class BytesImage {
  final Uint8List bytes;
  final String filename;
  final String type;

  BytesImage({
    this.bytes,
    this.filename,
    this.type,
  });
}

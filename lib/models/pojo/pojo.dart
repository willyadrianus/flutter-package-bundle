import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_package_bundle/models/json_constructor/json_constructor.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pojo.g.dart';

@JsonSerializable()
class Pojo extends JSONConstructor {
  final String name;
  final PojoData data;

  Pojo({
    this.name = "internal_load",
    @required this.data,
  });

  @override
  List<Object> get props => [
        name,
        data,
      ];

  @override
  Pojo fromJson(Map<String, dynamic> json) => _$PojoFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$PojoToJson(this);
}

@JsonSerializable()
class PojoData extends JSONConstructor {
  final String table;
  final List<PojoDomain> domains;
  final int offset;
  final int limit;
  final String orderBy;
  final int depthBrowse;
  final List<String> exceptionFields;

  PojoData({
    this.table,
    this.domains,
    this.offset,
    this.limit,
    this.orderBy,
    this.depthBrowse,
    this.exceptionFields,
  });

  @override
  PojoData fromJson(Map<String, dynamic> json) => _$PojoDataFromJson(json);

  @override
  List<Object> get props => [
        domains,
        table,
        offset,
        limit,
        orderBy,
        depthBrowse,
        exceptionFields,
      ];

  @override
  Map<String, dynamic> toJson() => _$PojoDataToJson(this);

  factory PojoData.fromJson(Map<String, dynamic> json) =>
      PojoData().fromJson(json);
}

@JsonSerializable()
class PojoDomain extends JSONConstructor {
  final String fieldName;
  final String operator;
  final dynamic value;
  final List<dynamic> values;

  PojoDomain({
    this.fieldName,
    this.operator,
    this.value,
    this.values,
  });

  @override
  PojoDomain fromJson(Map<String, dynamic> json) => _$PojoDomainFromJson(json);

  @override
  List<Object> get props => [
        fieldName,
        operator,
        value,
        values,
      ];

  @override
  Map<String, dynamic> toJson() => _$PojoDomainToJson(this);

  factory PojoDomain.fromJson(Map<String, dynamic> json) =>
      PojoDomain().fromJson(json);

  factory PojoDomain._parse(String domain) {
    if (domain == PojoOperator.and || domain == PojoOperator.or) {
      return PojoDomain(operator: domain);
    } else {
      List<String> data = domain.split(" ");
      if (data.length > 3) {
        if (data[1] == "not" && data[2] == "in") {
          data[1] = "not in";
          data[2] = data[3];
          data.removeAt(3);
        }
        if (data[1] == "in" || data[1] == "not in") {
          data[2] = data.sublist(2).join("");
        } else {
          data[2] = data.sublist(2).join(" ");
        }
      }

      if (data.length < 3) {
        return null;
      } else {
        dynamic value;
        if (data[2] == "null") {
          value = null;
        } else if (data[2].startsWith("\"") && data[2].endsWith("\"")) {
          value = data[2].substring(1, data[2].length - 1);
        } else if (data[2] == "true") {
          value = true;
        } else if (data[2] == "false") {
          value = false;
        } else {
          if (data[2].contains(".")) {
            value = double.tryParse(data[2]);
          }

          if (value == null) {
            value = int.tryParse(data[2]);
            if (value == null) {
              value = data[2];
            }
          }
        }

        List<String> valuesString = [];
        if (data[2].startsWith("[") &&
            data[2].endsWith("]") &&
            data[2].length > 2) {
          if (data[2].contains(",")) {
            valuesString = data[2].substring(1, data[2].length - 1).split(",");
          } else {
            valuesString = [data[2].substring(1, data[2].length - 1)];
          }
        }

        List<dynamic> values = [];
        for (int i = 0; i < valuesString.length; i++) {
          final value = valuesString[i];
          if (value == "null") {
            values.add(null);
          } else if (value.startsWith("\"") && value.endsWith("\"")) {
            values.add(value.substring(1, value.length - 1));
          } else if (value == "true") {
            values.add(true);
          } else if (value == "false") {
            values.add(false);
          } else {
            dynamic parseValue;
            if (value.contains(".")) {
              parseValue = double.tryParse(value);
            }

            if (parseValue == null) {
              parseValue = int.tryParse(value);
              if (parseValue == null) {
                parseValue = value;
              }
            }
            values.add(parseValue);
          }
        }

        return PojoDomain(
          fieldName: data[0],
          operator: data[1],
          value: value,
          values: values,
        );
      }
    }
  }
}

class PojoOperator {
  static const String equals = "=";
  static const String like = "like";
  static const String ins = "in";
  static const String notEquals = "!=";
  static const String and = "&";
  static const String or = "|";
  static const String openBracket = "(";
  static const String closeBracket = ")";
}

List<PojoDomain> domainsFromString(String domain) {
//    String domain = 's = "string" or i = 1 and d = 1.0 and b = true and o = string and ls = ["string1","string2"] and li = [1,2] and ld = [1.0,2.0] and lb = [true,false] and lo = [string,int]';
  String fixingDomain = domain;
  while (fixingDomain.contains("  ") ||
      fixingDomain.contains(" )") ||
      fixingDomain.contains("( ")) {
    if (fixingDomain.contains("  ")) {
      fixingDomain = fixingDomain.replaceAll("  ", " ");
    }
    if (fixingDomain.contains(" )")) {
      fixingDomain = fixingDomain.replaceAll(" )", ")");
    }
    if (fixingDomain.contains("( ")) {
      fixingDomain = fixingDomain.replaceAll("( ", "(");
    }
  }

  fixingDomain = fixingDomain.replaceAll(")", " )");
  fixingDomain = fixingDomain.replaceAll("(", "( ");
  String regexSplit = fixingDomain.splitMapJoin(
    RegExp("( and )|( or )|(\\(\\s)|(\\s\\))"),
    onMatch: (m) {
      return ";${m.group(0)};";
    },
    onNonMatch: (nm) {
      return "$nm";
    },
  );
  List<String> domains =
      regexSplit.split(";").where((element) => element != "").toList();
  for (int i = 0; i < domains.length; i++) {
    String domain = domains[i];
    if (domain == " )")
      domains[i] = PojoOperator.closeBracket;
    else if (domain == "( ")
      domains[i] = PojoOperator.openBracket;
    else if (domain == " and ")
      domains[i] = PojoOperator.and;
    else if (domain == " or ") domains[i] = PojoOperator.or;
  }
  List<String> prefixStep1 = domains.reversed.toList();
  List<String> postfixStep1 = prefixStep1 + [")"];
  ListQueue<String> _stacks = ListQueue()..addLast("(");
  List<String> _postfix = [];

  for (String expression in postfixStep1) {
    if (expression == PojoOperator.closeBracket)
      _stacks.addLast(PojoOperator.openBracket);
    else if (expression == PojoOperator.openBracket) {
      String expressionStack = _stacks.removeLast();
      while (expressionStack != PojoOperator.openBracket) {
        _postfix.add(expressionStack);
        expressionStack = _stacks.removeLast();
      }
    } else if (expression == PojoOperator.and ||
        expression == PojoOperator.or) {
      _stacks.addLast(expression);
    } else {
      _postfix.add(expression);
    }
  }

  while (_stacks.isNotEmpty) {
    String expressionStack = _stacks.removeLast();
    if (expressionStack != PojoOperator.openBracket)
      _postfix.add(expressionStack);
  }

  List<String> _prefix = _postfix.reversed.toList();
  if (!_prefix.contains(PojoOperator.or)) {
    _prefix.removeWhere((element) => element == PojoOperator.and);
  }
  List<PojoDomain> _pojoDomains = [];
  for (String domain in _prefix) {
    _pojoDomains.add(PojoDomain._parse(domain));
  }
  return _pojoDomains;
}

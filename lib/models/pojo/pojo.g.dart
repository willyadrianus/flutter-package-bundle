// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pojo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pojo _$PojoFromJson(Map<String, dynamic> json) {
  return Pojo(
    name: json['name'] as String,
    data: json['data'] == null
        ? null
        : PojoData.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PojoToJson(Pojo instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('name', instance.name);
  writeNotNull('data', instance.data?.toJson());
  return val;
}

PojoData _$PojoDataFromJson(Map<String, dynamic> json) {
  return PojoData(
    table: json['table'] as String,
    domains: (json['domains'] as List)
        ?.map((e) =>
            e == null ? null : PojoDomain.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    offset: json['offset'] as int,
    limit: json['limit'] as int,
    orderBy: json['order_by'] as String,
    depthBrowse: json['depth_browse'] as int,
    exceptionFields:
        (json['exception_fields'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$PojoDataToJson(PojoData instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('table', instance.table);
  writeNotNull('domains', instance.domains?.map((e) => e?.toJson())?.toList());
  writeNotNull('offset', instance.offset);
  writeNotNull('limit', instance.limit);
  writeNotNull('order_by', instance.orderBy);
  writeNotNull('depth_browse', instance.depthBrowse);
  writeNotNull('exception_fields', instance.exceptionFields);
  return val;
}

PojoDomain _$PojoDomainFromJson(Map<String, dynamic> json) {
  return PojoDomain(
    fieldName: json['field_name'] as String,
    operator: json['operator'] as String,
    value: json['value'],
    values: json['values'] as List,
  );
}

Map<String, dynamic> _$PojoDomainToJson(PojoDomain instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('field_name', instance.fieldName);
  writeNotNull('operator', instance.operator);
  writeNotNull('value', instance.value);
  writeNotNull('values', instance.values);
  return val;
}

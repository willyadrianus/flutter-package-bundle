/*
 * Abstract Class JSONConstructor for Converting Json To Object
 *
 * @author Willy Adrianus
 * @since 13-12-2019
 */

import 'package:equatable/equatable.dart';

abstract class JSONConstructor extends Equatable {
  dynamic fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson();

  List<R> fromAPI<R>(List<dynamic> json) {
    return json?.map<R>((map) => fromJson(map))?.toList() ?? [];
  }
}

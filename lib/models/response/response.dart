import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

/*
 * Response Class
 * by Willy
 *
 * Version Class 1.0.0
 *
 * Feature :
 * - [x] To Translate Response in Success, Empty Success, Failed
 * - [x] Only Parse / Change Data when Response is Success (Response.parse())
 * - [x] 'when' Response is EmptySuccess and empty body {}.. Change as Failed
 *
 * Dependency :
 * - [x] Translate Error Messages
 */

enum _Response {
  Success,
  EmptySuccess,
  Failed,
}

const String kResponseEmptySuccessErrorMessage = "Gagal Mengambil Data";

class Response<T> extends Equatable {
  final int count;
  final T data;
  final String errorMessage;
  final _Response type;

  Response._({this.count = 0, this.data, this.errorMessage, this.type});

  factory Response.success({T data}) => Response._(
        data: data,
        type: _Response.Success,
      );

  factory Response.emptySuccess() => Response._(
        errorMessage: kResponseEmptySuccessErrorMessage,
        type: _Response.EmptySuccess,
      );

  factory Response.failed({String errorMessage}) => Response._(
        errorMessage: errorMessage,
        type: _Response.Failed,
      );

  @override
  List<Object> get props => [type, data];

  bool get isSuccess => type == _Response.Success;

  bool get isEmptySuccess => type == _Response.EmptySuccess;

  bool get isNotEmpty => (data as List<dynamic>)[0]["count"] != 0;

  factory Response.parse({
    Response<dynamic> response,
    int count,
    T Function() data,
  }) =>
      Response._(
        count: count ?? 0,
        data: response.isSuccess ? data() : null,
        type: response.type,
        errorMessage: response.errorMessage,
      );

  factory Response.parseFromInternalLoad({
    Response response,
    T Function(List) generate,
  }) {
    if (response.isSuccess && !response.isNotEmpty) {
      return Response.emptySuccess();
    } else {
      return Response._(
        count: response.isSuccess ? response.data[0]["count"] : 0,
        data: response.isSuccess ? generate(response.data[0]["data"]) : null,
        type: response.type,
        errorMessage: response.errorMessage,
      );
    }
  }

  R when<R>({
    @required R Function() success,
    R Function() emptySuccess,
    @required R Function() failed,
  }) {
    switch (this.type) {
      case _Response.Success:
        return success?.call() ?? null;
      case _Response.EmptySuccess:
        return emptySuccess?.call() ?? failed?.call() ?? null;
      case _Response.Failed:
        return failed?.call() ?? null;
      default:
        return null;
    }
  }
}

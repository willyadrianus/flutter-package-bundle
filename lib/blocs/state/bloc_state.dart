import 'package:super_enum/super_enum.dart';

enum _BlocState {
  Init,
  Loading,
  Success,
  EmptySuccess,
  Failed,
}

@immutable
abstract class BlocState extends Equatable {
  const BlocState(this._type);

  factory BlocState.init() = Init;

  factory BlocState.loading() = Loading;

  factory BlocState.success() = Success;

  factory BlocState.emptySuccess() = EmptySuccess;

  factory BlocState.failed({@required String errorMessage}) = Failed;

  final _BlocState _type;

//ignore: missing_return
  FutureOr<R> when<R>(
      {FutureOr<R> Function(Init) init,
      @required FutureOr<R> Function(Loading) loading,
      @required FutureOr<R> Function(Success) success,
      FutureOr<R> Function(EmptySuccess) emptySuccess,
      @required FutureOr<R> Function(Failed) failed}) {
    assert(() {
      if (loading == null || success == null || failed == null)
        throw 'check for all possible cases';
      return true;
    }());
    switch (this._type) {
      case _BlocState.Init:
        return init?.call(this as Init) ?? loading(BlocState.loading());
      case _BlocState.Loading:
        return loading(this as Loading);
      case _BlocState.Success:
        return success(this as Success);
      case _BlocState.EmptySuccess:
        return emptySuccess?.call(this as EmptySuccess) ??
            failed(BlocState.failed(errorMessage: "Empty"));
      case _BlocState.Failed:
        return failed(this as Failed);
    }
  }

  FutureOr<R> whenOrElse<R>(
      {FutureOr<R> Function(Init) init,
      FutureOr<R> Function(Loading) loading,
      FutureOr<R> Function(Success) success,
      FutureOr<R> Function(EmptySuccess) emptySuccess,
      FutureOr<R> Function(Failed) failed,
      @required FutureOr<R> Function(BlocState) orElse}) {
    assert(() {
      if (orElse == null) throw 'Missing orElse case';
      return true;
    }());
    switch (this._type) {
      case _BlocState.Init:
        if (init == null) break;
        return init(this as Init);
      case _BlocState.Loading:
        if (loading == null) break;
        return loading(this as Loading);
      case _BlocState.Success:
        if (success == null) break;
        return success(this as Success);
      case _BlocState.EmptySuccess:
        if (emptySuccess == null) break;
        return emptySuccess(this as EmptySuccess);
      case _BlocState.Failed:
        if (failed == null) break;
        return failed(this as Failed);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(Init) init,
      FutureOr<void> Function(Loading) loading,
      FutureOr<void> Function(Success) success,
      FutureOr<void> Function(EmptySuccess) emptySuccess,
      FutureOr<void> Function(Failed) failed}) {
    assert(() {
      if (init == null &&
          loading == null &&
          success == null &&
          emptySuccess == null &&
          failed == null) throw 'provide at least one branch';
      return true;
    }());
    switch (this._type) {
      case _BlocState.Init:
        if (init == null) break;
        return init(this as Init);
      case _BlocState.Loading:
        if (loading == null) break;
        return loading(this as Loading);
      case _BlocState.Success:
        if (success == null) break;
        return success(this as Success);
      case _BlocState.EmptySuccess:
        if (emptySuccess == null) break;
        return emptySuccess(this as EmptySuccess);
      case _BlocState.Failed:
        if (failed == null) break;
        return failed(this as Failed);
    }
  }

  @override
  List get props => const [];
}

@immutable
class Init extends BlocState {
  const Init._() : super(_BlocState.Init);

  factory Init() {
    _instance ??= Init._();
    return _instance;
  }

  static Init _instance;
}

@immutable
class Loading extends BlocState {
  const Loading._() : super(_BlocState.Loading);

  factory Loading() {
    _instance ??= Loading._();
    return _instance;
  }

  static Loading _instance;
}

@immutable
class Success extends BlocState {
  const Success._() : super(_BlocState.Success);

  factory Success() {
    _instance ??= Success._();
    return _instance;
  }

  static Success _instance;
}

@immutable
class EmptySuccess extends BlocState {
  const EmptySuccess._() : super(_BlocState.EmptySuccess);

  factory EmptySuccess() {
    _instance ??= EmptySuccess._();
    return _instance;
  }

  static EmptySuccess _instance;
}

@immutable
class Failed extends BlocState {
  const Failed({@required this.errorMessage}) : super(_BlocState.Failed);

  final String errorMessage;

  @override
  String toString() => 'Failed(errorMessage:${this.errorMessage})';

  @override
  List get props => [errorMessage];
}

import 'dart:async';

import 'package:flutter_package_bundle/blocs/state/bloc_state.dart';
import 'package:flutter_package_bundle/blocs/load/load_bloc.dart';
import 'package:flutter_package_bundle/models/response/response.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

/*
 * Bloc untuk Form
 * by Willy
 *
 * Version Class 1.0.0
 *
 * Feature :
 * - [x] Auto Initial State = Init
 * - [x] Debounce Event
 * - [x] Validate for each Field
 * - [x] Validate for Form
 * - [x] Registering Outside Controller for Rebuild Validate Form
 */

abstract class FormBloc<E> extends CustomFormBloc<E, BlocState> {
  @override
  BlocState get initialState => BlocState.init();

  @override
  Stream<BlocState> transformEvents(
    Stream<E> events,
    Stream<BlocState> Function(E event) next,
  ) {
    final nonDebounceStream = events.where((event) {
      return !includeDebounceStream(event);
    });
    final debounceStream = events.where((event) {
      return includeDebounceStream(event);
    }).debounceTime(
      Duration(milliseconds: durationDebounceInMilliseconds),
    );
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      next,
    );
  }

  @override
  Stream<BlocState> whenSuccess(
    E event,
    Response response,
  ) async* {
    yield BlocState.success();
  }

  @override
  Stream<BlocState> whenFailed(
    E event,
    Response response,
  ) async* {
    yield BlocState.failed(errorMessage: response.errorMessage);
  }
}

abstract class CustomFormBloc<E, S> extends CustomBloc<E, S> {
  // Form Valid
  final _formValid = BehaviorSubject<bool>();

  Stream<bool> get formValid => _formValid.stream;

  bool _lastValid = false;

  // Validate Error
  List<ValidateStream> validateStreams;

  ValidateStream getStream(String key) =>
      validateStreams?.firstWhere(
        (stream) => stream.key.toLowerCase() == key.toLowerCase(),
        orElse: () => null,
      ) ??
      null;

  Stream<S> validate<T>(String key, T value) async* {
    ValidateStream stream = getStream(key);
    if (stream != null) {
      stream.sink(value);
      _updateFormValid();
      yield initialState;
    }
  }

  bool _isValidStreams() {
    bool isValid = true;
    for (ValidateStream stream in validateStreams ?? []) {
      if (!(stream.isValid(stream.value))) {
        isValid = false;
        break;
      }
    }
    return isValid;
  }

  void _updateFormValid() {
    _lastValid = _isValidStreams();
    _reloadFormValid();
  }

  // For Reload Form Valid Stream when Outside Controller is updated
  void _reloadFormValid() => _formValid.sink.add(_lastValid);

  void registerOutsideController(List<ChangeNotifier> controllers) =>
      controllers.forEach(
        (controller) => controller.addListener(_reloadFormValid),
      );

  @override
  Future<void> close() async {
    for (ValidateStream stream in validateStreams ?? []) await stream.close;
    await _formValid.close();
    await super.close();
  }

  // Debounce Event
  int get durationDebounceInMilliseconds => 300;

  bool includeDebounceStream(E event) => false;
}

class ValidateStream<T> extends Equatable {
  final String key;
  final bool Function(T) isValid;
  final String Function(T) validate;
  final BehaviorSubject<T> _streamValueController = BehaviorSubject<T>();
  final BehaviorSubject<String> _streamController = BehaviorSubject<String>();

  ValidateStream({this.key, this.isValid, this.validate});

  Stream<String> get stream => _streamController.stream;

  T get value => _streamValueController.value;

  Future<dynamic> get close async {
    await _streamValueController.close();
    await _streamController.close();
  }

  void sink(T value) {
    _streamValueController.sink.add(value);
    _streamController.sink.add(validate(value));
  }

  @override
  List<Object> get props => [key];
}

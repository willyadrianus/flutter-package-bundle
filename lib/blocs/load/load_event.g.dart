// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'load_event.dart';

// **************************************************************************
// SuperEnumGenerator
// **************************************************************************

@immutable
abstract class LoadEvent extends Equatable {
  const LoadEvent(this._type);

  factory LoadEvent.firstLoad() = FirstLoad;

  factory LoadEvent.reload() = Reload;

  factory LoadEvent.search({@required String query}) = Search;

  final _LoadEvent _type;

//ignore: missing_return
  FutureOr<R> when<R>(
      {@required FutureOr<R> Function(FirstLoad) firstLoad,
      @required FutureOr<R> Function(Reload) reload,
      @required FutureOr<R> Function(Search) search}) {
    assert(() {
      if (firstLoad == null || reload == null || search == null)
        throw 'check for all possible cases';
      return true;
    }());
    switch (this._type) {
      case _LoadEvent.FirstLoad:
        return firstLoad(this as FirstLoad);
      case _LoadEvent.Reload:
        return reload(this as Reload);
      case _LoadEvent.Search:
        return search(this as Search);
    }
  }

  FutureOr<R> whenOrElse<R>(
      {FutureOr<R> Function(FirstLoad) firstLoad,
      FutureOr<R> Function(Reload) reload,
      FutureOr<R> Function(Search) search,
      @required FutureOr<R> Function(LoadEvent) orElse}) {
    assert(() {
      if (orElse == null) throw 'Missing orElse case';
      return true;
    }());
    switch (this._type) {
      case _LoadEvent.FirstLoad:
        if (firstLoad == null) break;
        return firstLoad(this as FirstLoad);
      case _LoadEvent.Reload:
        if (reload == null) break;
        return reload(this as Reload);
      case _LoadEvent.Search:
        if (search == null) break;
        return search(this as Search);
    }
    return orElse(this);
  }

  FutureOr<void> whenPartial(
      {FutureOr<void> Function(FirstLoad) firstLoad,
      FutureOr<void> Function(Reload) reload,
      FutureOr<void> Function(Search) search}) {
    assert(() {
      if (firstLoad == null && reload == null && search == null)
        throw 'provide at least one branch';
      return true;
    }());
    switch (this._type) {
      case _LoadEvent.FirstLoad:
        if (firstLoad == null) break;
        return firstLoad(this as FirstLoad);
      case _LoadEvent.Reload:
        if (reload == null) break;
        return reload(this as Reload);
      case _LoadEvent.Search:
        if (search == null) break;
        return search(this as Search);
    }
  }

  @override
  List get props => const [];
}

@immutable
class FirstLoad extends LoadEvent {
  const FirstLoad._() : super(_LoadEvent.FirstLoad);

  factory FirstLoad() {
    _instance ??= FirstLoad._();
    return _instance;
  }

  static FirstLoad _instance;
}

@immutable
class Reload extends LoadEvent {
  const Reload._() : super(_LoadEvent.Reload);

  factory Reload() {
    _instance ??= Reload._();
    return _instance;
  }

  static Reload _instance;
}

@immutable
class Search extends LoadEvent {
  const Search({@required this.query}) : super(_LoadEvent.Search);

  final String query;

  @override
  String toString() => 'Search(query:${this.query})';
  @override
  List get props => [query];
}

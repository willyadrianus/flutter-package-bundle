import 'package:bloc/bloc.dart';
import 'package:flutter_package_bundle/models/response/response.dart';

import '../state/bloc_state.dart';
import 'load_event.dart';

/*
 * Bloc untuk Load Data
 * by Willy
 *
 * Version Class 1.2.0
 *
 * Feature :
 * - [x] Auto Initial State = Init
 * - [x] Auto Loading when Event is Added
 * - [x] First Load & Reload Event
 * - [x] Search Event
 * - [x] Option Offset, Limit & Pagination
 */

abstract class LoadEventBloc extends LoadBloc<LoadEvent> {
  @override
  Stream<BlocState> mapEventToState(LoadEvent event) async* {
    yield BlocState.loading();
    yield* await event.when(
      firstLoad: mapFirstLoadToState,
      reload: mapReloadToState,
      search: mapSearchToState,
    );
  }

  Stream<BlocState> mapFirstLoadToState(LoadEvent event) async* {
    yield* load(event);
  }

  Stream<BlocState> mapReloadToState(LoadEvent event) async* {
    yield* load(event);
  }

  Stream<BlocState> load(LoadEvent event);

  Stream<BlocState> mapSearchToState(Search event) async* {
    yield BlocState.success();
  }
}

abstract class LoadBloc<E> extends CustomBloc<E, BlocState> with _Pagination {
  @override
  BlocState get initialState => BlocState.init();

  Stream<BlocState> whenFailed(
    E event,
    Response response,
  ) async* {
    yield BlocState.failed(errorMessage: response.errorMessage);
  }
}

abstract class CustomBloc<E, S> extends Bloc<E, S> {
  Stream<S> whenSuccess(E event, Response response);

  Stream<S> whenFailed(E event, Response response);
}

class _Pagination {
  int count = 0;
  int showPageCount = 3;
  int limit = 10;

  // Start from 0
  int _startPage = 0;

  bool allowZeroPage = true;

  int get offset => _startPage * limit;

  int get currentPage => _startPage + 1;

  int get totalPage {
    if (allowZeroPage && (count / limit).ceil() == 0) {
      return 1;
    }
    return (count / limit).ceil();
  }

  bool get isFirstPage => currentPage == 1;

  bool get isLastPage => (currentPage == totalPage);

  bool get isNearLastPage => (currentPage >= totalPage - showPageCount + 1);

  void setPage(int page) => _startPage = page;

  void toFirstPage() => _startPage = 0;

  void toPreviousPage() => _startPage--;

  void toNextPage() => _startPage++;

  void toLastPage() => _startPage = totalPage - 1;
}

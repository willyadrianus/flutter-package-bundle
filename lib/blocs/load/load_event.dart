import 'package:super_enum/super_enum.dart';

part 'load_event.g.dart';

@superEnum
enum _LoadEvent {
  @object
  FirstLoad,
  @object
  Reload,
  @Data(fields: [
    DataField("query", String),
  ])
  Search,
}

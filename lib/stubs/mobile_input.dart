import 'package:flutter_package_bundle/models/bytes_image/bytes_image.dart';

import 'input_interface.dart';

class MobileInput implements Input {
  @override
  Future<void> chooseFile({
    Function(BytesImage) addImage,
    String data = "image",
  }) async {
    // Do Nothing
  }
}

Input getInput() => MobileInput();

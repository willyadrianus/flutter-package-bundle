import 'package:flutter_package_bundle/models/bytes_image/bytes_image.dart';

import 'input_stub.dart'
// ignore: uri_does_not_exist
    if (dart.library.io) 'package:flutter_package_bundle/stubs/mobile_input.dart'
// ignore: uri_does_not_exist
    if (dart.library.html) 'package:flutter_package_bundle/stubs/web_input.dart';

abstract class Input {
  Future<void> chooseFile({
    Function(BytesImage) addImage,
    String data = "image",
  }) async {}

  /// factory constructor to return the correct implementation.
  factory Input() => getInput();
}

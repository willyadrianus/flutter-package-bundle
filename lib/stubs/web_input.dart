import 'dart:convert';
import 'dart:html';

import 'package:flutter_package_bundle/models/bytes_image/bytes_image.dart';

import 'input_interface.dart';

class WebInput implements Input {
  @override
  Future<void> chooseFile({
    Function(BytesImage) addImage,
    String data = "image",
  }) async {
    InputElement uploadInput = FileUploadInputElement();
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      final files = uploadInput.files;
      if (files.length == 1) {
        for (File file in files) {
          final reader = FileReader();

          reader.onLoadEnd.listen((e) async {
            String _base64 = reader.result;
            final stripped = (data == "application")
                ? _base64.replaceFirst(
                    RegExp(r'data:application/[^;]+;base64,'), '')
                : _base64.replaceFirst(RegExp(r'data:image/[^;]+;base64,'), '');
            final base64Code = Base64Codec();
            final bytes = base64Code.decode(stripped);
            addImage?.call(BytesImage(
              bytes: bytes,
              filename: file.name,
              type: file.name.split(".").last,
            ));
          });

          reader.readAsDataUrl(file);
        }
      }
    });
  }
}

Input getInput() => WebInput();

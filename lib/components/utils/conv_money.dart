import 'package:intl/intl.dart';

class ConvMoney {
  static String toRupiah(value) {
    return NumberFormat.currency(
      locale: 'in_ID',
      symbol: 'Rp ',
      decimalDigits: ((value % 1) as num).abs() <= (0.0000001 * 100) ? 0 : 2,
    ).format(value);
  }
}

import 'package:flutter/material.dart';

/// Widget untuk membuat space
/// Digunakan untuk membuat responsive saat menggunakan column dan row
///
/// 6 Type of constructor based by "EdgeInsets"
/// [all], [vertical], [horizontal], [symmetric], [only], [fromLTRW]
///
/// Dependency:
/// Resize.dart

class Space extends StatelessWidget {
  final double left, right, top, bottom;
  final Widget child;

  Space(this.left, this.right, this.top, this.bottom, {this.child});

  factory Space.of(double value, {Widget child}) {
    return Space(
      value,
      value,
      value,
      value,
      child: child,
    );
  }

  factory Space.all(double value, {Widget child}) {
    return Space(
      value,
      value,
      value,
      value,
      child: child,
    );
  }

  factory Space.vertical(double value, {Widget child}) {
    return Space(
      0,
      0,
      value,
      value,
      child: child,
    );
  }

  factory Space.horizontal(double value, {Widget child}) {
    return Space(
      value,
      value,
      0,
      0,
      child: child,
    );
  }

  factory Space.symmetric(
      {double vertical = 0, double horizontal = 0, Widget child}) {
    return Space(
      horizontal,
      horizontal,
      vertical,
      vertical,
      child: child,
    );
  }

  factory Space.only(
      {double left = 0,
      double right = 0,
      double top = 0,
      double bottom = 0,
      Widget child}) {
    return Space(
      left,
      right,
      top,
      bottom,
      child: child,
    );
  }

  factory Space.fromLTRW(double left, double top, double right, double bottom,
      {Widget child}) {
    return Space(
      left,
      right,
      top,
      bottom,
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    if (child == null) {
      return Container(
        height: top + bottom,
        width: left + right,
      );
    }
    else {
      return Container(
        padding: EdgeInsets.only(
          left: left,
          right: right,
          top: top,
          bottom: bottom,
        ),
        child: child,
      );
    }
  }
}

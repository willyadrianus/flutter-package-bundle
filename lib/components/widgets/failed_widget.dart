import 'package:flutter/material.dart';
import 'package:flutter_package_bundle/components/widgets/space.dart';

class FailedWidget extends StatelessWidget {
  final Function() reload;
  final String errorMessage;

  FailedWidget(
    this.errorMessage, [
    this.reload,
  ]);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: reload?.call,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.error,
              color: Colors.red,
              size: 50,
            ),
            Space.of(5),
            Text(
              errorMessage,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            reload == null ? Container() : _buildTapToRetry(),
          ],
        ),
      ),
    );
  }

  Widget _buildTapToRetry() {
    return Column(
      children: <Widget>[
        Space.of(5),
        Text(
          "Tap To Retry",
          style: TextStyle(
            fontSize: 16,
            color: Colors.blue[400],
            decoration: TextDecoration.underline,
          ),
        ),
      ],
    );
  }
}

class FailedContent extends StatelessWidget {
  final String errorMessage;

  FailedContent({this.errorMessage});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.red[800],
          ),
          child: Text(
            errorMessage ?? "",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
        Space.of(5),
      ],
    );
  }
}

import 'package:flutter/material.dart';

class CircleContainer extends StatelessWidget {
  final Color color;
  final double size;
  final Color borderColor;
  final double borderSize;
  final AlignmentGeometry alignment;
  final Widget child;
  final EdgeInsetsGeometry padding;

  CircleContainer({
    @required this.size,
    this.padding = const EdgeInsets.all(0),
    this.color = Colors.transparent,
    this.borderColor = Colors.transparent,
    this.borderSize = 1.0,
    this.alignment,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      width: size,
      height: size,
      alignment: alignment,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
        border: Border.all(
          color: borderColor,
          width: borderSize,
        ),
      ),
      child: child ?? Container(),
    );
  }
}

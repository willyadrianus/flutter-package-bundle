import 'dart:typed_data';

import 'package:flutter/material.dart';

/*
 * Build Image Widget
 * by Willy
 *
 * Version Class 1.1.0
 *
 * Feature :
 * - [x] For Build Image either from Asset or Network or Bytes
 * - [x] Placeholder Image
 * - [x] Box Fit
 * - [x] Color
 * - [x] Setting Size
 * - [x] Init Set No Image Asset
 *
 */

enum ImageType {
  NETWORK,
  ASSET,
  BYTES,
}

class BuildImage extends StatelessWidget {

  static void init({String defaultNoImageAsset}) {
    _defaultNoImageAsset = defaultNoImageAsset;
  }
  
  static String _defaultNoImageAsset;
  
  final String imageUrl;
  final double width;
  final double height;
  final Widget placeholder;
  final ImageType imageType;
  final BoxFit fit;
  final Color color;
  final Uint8List bytes;
  final Alignment alignment;

  BuildImage(
    this.imageUrl, {
    this.width,
    this.height,
    this.placeholder,
    this.imageType = ImageType.NETWORK,
    this.fit = BoxFit.contain,
    this.color,
    this.bytes,
    this.alignment,
  });

  factory BuildImage.box(
    String imageUrl,
    double size, {
    Widget placeholder,
    ImageType imageType,
    BoxFit fit,
    Color color,
  }) {
    return BuildImage(
      imageUrl,
      width: size,
      height: size,
      placeholder: placeholder,
      imageType: imageType ?? ImageType.NETWORK,
      fit: fit,
      color: color,
    );
  }

  factory BuildImage.bytesBox(
    Uint8List bytes,
    double size, {
    Widget placeholder,
    BoxFit fit,
    Color color,
  }) {
    return BuildImage(
      "",
      bytes: bytes,
      width: size,
      height: size,
      placeholder: placeholder,
      imageType: ImageType.BYTES,
      fit: fit,
      color: color,
    );
  }

  factory BuildImage.bytes(
    Uint8List bytes, {
    double width,
    double height,
    Widget placeholder,
    BoxFit fit,
    Color color,
  }) {
    return BuildImage(
      "",
      bytes: bytes,
      width: width,
      height: height,
      placeholder: placeholder,
      imageType: ImageType.BYTES,
      fit: fit,
      color: color,
    );
  }

  @override
  Widget build(BuildContext context) {
    if ((imageUrl?.isEmpty ?? true) && imageType != ImageType.BYTES)
      return _buildPlaceholder();
    if (width != null || height != null) {
      return Container(
        width: width,
        height: height,
        alignment: alignment ?? Alignment.center,
        child: imageType == ImageType.ASSET
            ? _buildImageAsset()
            : imageType == ImageType.NETWORK
                ? _buildImageNetwork()
                : _buildImageBytes(),
      );
    } else {
      return imageType == ImageType.ASSET
          ? _buildImageAsset()
          : imageType == ImageType.NETWORK
              ? _buildImageNetwork()
              : _buildImageBytes();
    }
  }

  Widget _buildImageAsset() {
    return Image.asset(
      imageUrl,
      fit: fit,
      color: color,
    );
  }

  Widget _buildImageNetwork() {
    return Image.network(
      imageUrl,
      fit: fit,
      color: color,
    );
  }

  Widget _buildImageBytes() {
    return Image.memory(
      bytes,
      fit: fit,
      color: color,
    );
  }

  Widget _buildPlaceholder() {
    if (placeholder != null) return placeholder;
    return (width != null && height != null)
        ? Container(
            width: width,
            height: height,
            child: _buildNoImage(),
          )
        : _buildNoImage();
  }

  Widget _buildNoImage() {
    if (_defaultNoImageAsset?.isNotEmpty ?? false) {
      return Image.asset(
        _defaultNoImageAsset,
        fit: fit,
      );
    }
    else return Container();
  }
}

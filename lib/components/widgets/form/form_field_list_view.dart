import 'package:flutter/material.dart';
import 'package:flutter_package_bundle/material_bundle.dart';

class FormFieldListView extends StatelessWidget {
  final String fieldName;
  final List<String> values;

  FormFieldListView({this.fieldName, this.values = const []});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 110,
          child: Text(
            fieldName ?? "",
            style: TextStyle(
              fontSize: 14,
            ),
          ),
        ),
        Space.of(5),
        Expanded(
          child: Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: BorderRadius.circular(5),
            ),
            child: Row(
              children: values.map((v) {
                return Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colors.grey[400],
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Text(
                        v,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Space.of(2),
                  ],
                );
              }).toList(),
            ),
          ),
        ),
      ],
    );
  }
}

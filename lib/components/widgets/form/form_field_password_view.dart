import 'package:flutter/material.dart';
import 'package:flutter_package_bundle/material_bundle.dart';

class FormFieldPasswordView extends StatelessWidget {
  final String fieldName;
  final String hintText;
  final TextEditingController controller;

  FormFieldPasswordView({this.fieldName, this.hintText, this.controller});

  final _obscureText = ValueNotifier(true);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 75,
          child: Text(
            fieldName ?? "Password",
            style: TextStyle(
              fontSize: 14,
            ),
          ),
        ),
        Space.of(5),
        Expanded(
          child: ValueListenableBuilder(
            valueListenable: _obscureText,
            builder: (context, obscureText, child) {
              return TextFormField(
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  hintText: hintText ?? fieldName ?? "Password",
                  hintStyle: TextStyle(
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                  suffixIcon: GestureDetector(
                    onTap: () => _obscureText.value = !obscureText,
                    child: Container(
                      child: Icon(
                        obscureText ? Icons.visibility : Icons.visibility_off,
                      ),
                    ),
                  ),
                ),
                controller: controller,
                obscureText: obscureText,
              );
            },
          ),
        ),
      ],
    );
  }
}
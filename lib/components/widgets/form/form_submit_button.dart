import 'package:flutter/material.dart';

class FormSubmitButton extends StatelessWidget {
  final String name;
  final Function onPressed;
  FormSubmitButton({this.onPressed, this.name});
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      color: Colors.blue,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      child: Text(
        name ?? "Simpan",
        style: TextStyle(
          fontSize: 18,
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

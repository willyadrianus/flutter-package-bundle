import 'package:flutter/material.dart';
import 'package:flutter_package_bundle/material_bundle.dart';

class FormFieldView extends StatelessWidget {
  final String fieldName;
  final String value;
  final int multiLine;
  final String hintText;
  final TextEditingController controller;
  final Function() onPressed;
  final FocusNode focus;
  final bool autoFocus;
  final bool enabled;
  final bool readOnly;
  final double height;
  final EdgeInsetsGeometry contentPadding;
  final double width;

  FormFieldView({
    this.fieldName,
    this.value,
    this.multiLine,
    this.hintText,
    this.onPressed,
    this.controller,
    this.focus,
    this.autoFocus,
    this.enabled,
    this.readOnly,
    this.height,
    this.contentPadding,
    this.width,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        if (fieldName != null) ...[
          Container(
            width: width ?? 75,
            child: Text(
              fieldName,
              style: TextStyle(
                fontSize: 14,
              ),
            ),
          ),
          Space.of(5),
        ],
        Expanded(
          child: Container(
            height: height,
            child: onPressed != null ? _buildText() : _buildTextFormField(),
          ),
        ),
      ],
    );
  }

  Widget _buildText() {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 20,
        ),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Text(
          value ?? hintText ?? fieldName ?? "",
          style: TextStyle(
            color: value != null ? Colors.black : Colors.grey,
            fontSize: 12,
            fontWeight: FontWeight.w400,
          ),
          maxLines: multiLine ?? 1,
        ),
      ),
    );
  }

  Widget _buildTextFormField() {
    return TextFormField(
      initialValue: value,
      style: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w400,
      ),
      decoration: InputDecoration(
        contentPadding: contentPadding,
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(5),
        ),
        hintText: hintText ?? fieldName ?? "",
        hintStyle: TextStyle(
          fontSize: 14,
          color: Colors.grey,
        ),
      ),
      enabled: enabled ?? true,
      readOnly: readOnly ?? false,
      controller: controller,
      focusNode: focus,
      autofocus: autoFocus ?? false,
      maxLines: multiLine ?? 1,
    );
  }
}

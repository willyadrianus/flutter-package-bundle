import 'package:flutter/material.dart';
import 'package:flutter_package_bundle/material_bundle.dart';
import 'package:flutter_package_bundle/models/bytes_image/bytes_image.dart';
import 'package:flutter_package_bundle/stubs/input_interface.dart';

class FormFieldUploadView extends StatelessWidget {
  final String fieldName;
  final Function(BytesImage) addImage;

  /* default image
   * final stripped = _base64.replaceFirst(RegExp(r'data:$data/[^;]+;base64,'), '');
   *
   * Option :
   * image (JPG, PNG)
   * application (PDF)
   */
  final String data;

  FormFieldUploadView({this.fieldName, this.addImage, this.data = "image"});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        GestureDetector(
          onTap: () => _chooseFile(),
          child: Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.file_upload,
                  color: Colors.grey,
                ),
                Space.of(2),
                Text(
                  "Upload Files",
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
        ),
        Space.of(10),
        Expanded(
          child: Text(
            fieldName,
            style: TextStyle(
              fontSize: 16,
              color: Colors.grey,
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _chooseFile() async {
    await Input().chooseFile(
      addImage: addImage,
      data: data,
    );
  }
}

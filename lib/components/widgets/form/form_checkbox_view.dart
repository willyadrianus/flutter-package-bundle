import 'package:flutter/material.dart';
import 'package:flutter_package_bundle/material_bundle.dart';

class FormCheckboxView extends StatelessWidget {
  final String fieldName;
  final bool checked;
  final double width;
  final String position; //left, right

  FormCheckboxView({
    this.fieldName,
    this.checked = false,
    this.width,
    this.position = "left",
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        if ((fieldName?.isNotEmpty ?? false) && position == "left") ...[
          Container(
            width: width ?? 110,
            child: Text(
              fieldName,
              style: TextStyle(
                fontSize: 14,
              ),
            ),
          ),
          Space.of(5),
        ],
        checked
            ? Icon(
                Icons.check_box,
                color: Colors.blue,
              )
            : Icon(
                Icons.check_box,
                color: Colors.grey,
              ),
        if ((fieldName?.isNotEmpty ?? false) && position != "left") ...[
          Space.of(5),
          Text(
            fieldName,
            style: TextStyle(
              fontSize: 14,
            ),
          ),
        ],
      ],
    );
  }
}
